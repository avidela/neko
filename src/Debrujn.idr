module Debrujn

import Data.Nat
import Equality
import Decidable.Equality

%default total

prefix 9 \\
prefix 9 #

infixr 0 ==> -- function application, like $
infixr 4 ~>  -- Single Reduction step
infixr 2 ->> -- "Reduces to"
infixr 7 =>> -- Type in Lambda calculus

infix 5 :-

infixl 6 .:
infixr 4 >:
infixl 5 &.
infixl 5 |-

public export 0
funExt : {f, g : a -> b} -> ((x : a) -> f x = g x) ->  f = g

public export 0
funExt2 : {f, g : a -> b -> c} -> ((x : a) -> (y : b) -> f x y = g x y) ->  f = g

-- lambda type
public export
data Ty : (0 _ : Type) -> Type where
  (=>>) : Ty b -> Ty b -> Ty b
  Base : base -> Ty base
  Unit : Ty b

-- Context
public export
data Ctxt : Type -> Type where
  Nil : Ctxt b
  (&.) : Ctxt b -> Ty b -> Ctxt b

length : Ctxt b -> Nat
length [] = 0
length (g &. _) = S (length g)

public export
data (>:) : {0 base : Type} -> Ctxt base -> Ty base -> Type where
  Z : gam &. a >: a
  S : (0 extra : Ty base) -> gam >: a -> gam &. extra >: a

public export
data (|-) : {0 base : Type} -> Ctxt base -> Ty base -> Type where

  Var : {0 Γ : _} ->
        Γ >: a ->
     --------------
        Γ |- a

  Lam : {0 Γ : _} ->
        {a, b : Ty base} ->
        Γ &. a |- b ->
     --------------------
        Γ |- a =>> b

  App : {0 Γ : _} -> {a, b : Ty base} ->
         Γ |- a  =>> b ->
         Γ |- a ->
      ------------------
         Γ |- b

lookup : (gam : Ctxt b) -> (n : Nat) -> (p : n `LT` length gam) -> Ty b
lookup [] 0 p = absurd p
lookup (_ &. y) 0 (LTESucc z) = y
lookup [] (S k) p = absurd p
lookup (x &. _) (S k) (LTESucc z) = lookup x k z

count : (n : Nat) -> (gam : Ctxt b) -> (p : n `LT` length gam) -> gam >: lookup gam n p
count 0 [] p = absurd p
count 0 (_ &. _) (LTESucc z) = Z
count (S k) [] p = absurd p
count (S k) (x &. y) (LTESucc z) = S _ (count k x z)

(#) :  (n : Nat) -> {gam : Ctxt b} -> {auto inGam : n `LT` length gam}
   -> gam |- lookup gam n inGam
(#) n = Var (count n gam inGam)

public export 0
Subset : (f : a -> b -> Type) -> (x : a) -> (y : a) -> Type
Subset f x y = (0 xsub : b) -> f x xsub -> f y xsub

public export 0
Rename : Ctxt base -> Ctxt base -> Type
Rename c1 c2 = Subset (>:) c2 c1

public export 0
CtxtMap : Ctxt base -> Ctxt base -> Type
CtxtMap gam del = Subset (|-) del gam

public export 0
Subst : (gam, del : Ctxt base) -> Type
Subst gam del = (0 a' : Ty base) -> del >: a' -> gam |- a'

public export
extend : {0 gam, del: Ctxt base}
      -> {b : Ty base}
      -> Rename del gam
      -> del &. b `Rename` gam &. b
extend f b Z = Z
extend f xsub (S _ x) = S _ (f xsub x)

public export
forget : (ds `Rename` gs &. g) -> ds `Rename` gs
forget f xsub Z = f xsub (S g Z)
forget f xsub (S extra x) = f xsub (S g (S extra x))

public export
prfForget : {0 sub : del `Rename` gam &. g } -> (n : gam >: t) ->
            (forget sub) t n === sub t (S g n)
prfForget Z = Refl
prfForget (S extra x) = Refl

public export 0
forgetTwice' : {gam, del : Ctxt base} ->
               {g : Ty base} ->
               {sub : del `Rename` gam &. g} ->
               (extend {b=g} (forget {g} sub)) = (forget {g} (extend {b=g} sub))

public export 0
forgetTwice : {gam, del : Ctxt base} ->
              {sub : del `Rename` gam &. g} ->
              forget (extend (forget sub)) = forget (forget (extend sub))

public export
rename : {0 gam, del: Ctxt base}
      -> del `Rename` gam
      -> del `CtxtMap` gam
rename f t (Var x) = Var (f t x)
rename f (a =>> b) (Lam x) = Lam (rename (extend f) b x)
rename f b (App x {a} y {b}) = App (rename f (a =>> b) x) (rename f a y)

-- subZero : (sub : del `Subst` gs &. g ) -> rename (\_ => S g) (sub g Z) = ?rhs

0
renVar : {val : Ty base} -> {gam, del : Ctxt base} -> {sub : Subst del (gs &. g)} ->
         rename (\v' => S val) g (sub g Z) = Var (S val Z)
renVar {g} with (sub g Z)
  renVar {g = g} | (Var x) = ?nanit
  renVar {g = (a =>> b)} | (Lam x) = ?renVar_rhs_2
  renVar {g = g} | (App x y) = ?renVar_rhs_3

public export 0
renApp : rename sub b (App x y) = App (rename sub (a =>> b) x) (rename sub a y)
renApp {b = (z =>> w)} = Refl
renApp {b = (Base z)} = Refl
renApp {b = Unit} = Refl

public export
extendJ : {0 gam, del : Ctxt base} ->
          Subst del gam ->
          (0 b : Ty base) ->
          del &. b `Subst` gam &. b
extendJ sub a a Z = Var Z
extendJ sub b a (S b x) =
    rename {del= del &. b} (\v => S {gam=del} {a=v} b) (_) (sub a x)

||| simultaneous substitution
public export
subst : {0 gam, del : Ctxt base} ->
        del `Subst` gam ->
        del `CtxtMap` gam
subst f xsub (Var x) = f _ x
subst f (a =>> b) (Lam x) =
  Lam (subst (extendJ f a) _ x)
subst f xsub (App x y) =
  App (subst f _ x) (subst f _ y)


public export
Sigma : {0 x, b : Ty base} -> gam |- b -> gam &. b >: x  -> gam |- x
Sigma y Z = y
Sigma y (S _ z) = Var z

public export
(:-) : {gam : _}
    -> {a, b : Ty base}
    -> gam &. b |- a
    -> gam |- b
    -> gam |- a
(:-) x y =
   Debrujn.subst (\c => Sigma y {x=c}) a x


public export
proofApp : (b' : Ty base) ->
           (f : (gam &. a =>> b) |- b')->
           (n : (gam &. a) |- b) ->
           App (Lam f) (Lam n) = subst (\0 c => Sigma (Lam n)) b' f
proofApp (a =>> b) (Var Z) n = ?proofApp_rhs_1
proofApp b' (Var (S _ x)) n = ?proofApp_rhs_34
proofApp (a =>> b) (Lam x) n = ?proofApp_rhs_2
proofApp b' (App x y) n = ?proofApp_rhs_3

public export
data Value : gam |- a -> Type where
  VLam : Value (Lam n)

--  VZero : Value Zero
--  VSucc : Value v -> Value (Succ v)

public export
data (~>) : (t1, t2 : gam |- a) -> Type where
  Xi1 : l ~> l' -> App l m ~> App l' m
  Xi2 : {v : gam |- a =>> b} -> {m, m' : gam |- a} ->
        Value v -> m ~> m' -> App v m ~> App v m'
  BetaLam : {f : gam &. b |- a} -> {v : gam |- b} ->
            Value v -> (App (Lam f) v) ~> (f :- v)

applyIdentity : {gam : Ctxt base} -> {x : gam |- b} -> Value x -> (App (Lam (Var Z)) x) ~> x
applyIdentity = BetaLam

public export
data (->>) : (t1, t2 : gam |- a) -> Type where
  Step : m ~> n -> m ->> n
  ReduceRefl : (t : gam |- a) -> t ->> t
  ReduceTrans : l ->> m -> m ->> n -> l ->> n

{-
implementation Preorder (gam |- a) (->>) where
  reflexive = ReduceRefl
  transitive a b c ab bc = ReduceTrans ab bc

twoIsTwo : Debrujn.cTwo |> Debrujn.cSuc |> Zero ->> Succ (Succ Zero)
twoIsTwo = begin (->>) $
           cTwo |> cSuc |> Zero
           -< Step (Xi1 (BetaLam VLam)) >-
           \\ (cSuc |> (cSuc |> #0)) |> Zero
           -< Step (BetaLam VZero) >-
           (cSuc |> (cSuc |> Zero))
           -< Step (Xi2 VLam (BetaLam VZero)) >-
           cSuc |> Succ Zero
           -< Step (BetaLam (VSucc VZero)) >-
           End (Succ (Succ Zero))

valuesWontReduce : Value v -> Not (v ~> v')
valuesWontReduce VLam (Xi1 _) impossible
valuesWontReduce VZero (Xi1 _) impossible
valuesWontReduce (VSucc x) (Xi1 _) impossible
valuesWontReduce VLam (Xi2 _ _) impossible
valuesWontReduce VZero (Xi2 _ _) impossible
valuesWontReduce (VSucc x) (Xi2 _ _) impossible
valuesWontReduce VLam (BetaLam _) impossible
valuesWontReduce VZero (BetaLam _) impossible
valuesWontReduce (VSucc x) (BetaLam _) impossible
valuesWontReduce (VSucc x) (XiSucc y) = valuesWontReduce x y
valuesWontReduce VLam (XiCase _) impossible
valuesWontReduce VZero (XiCase _) impossible
valuesWontReduce (VSucc x) (XiCase _) impossible
valuesWontReduce VLam BetaZero impossible
valuesWontReduce VZero BetaZero impossible
valuesWontReduce (VSucc x) BetaZero impossible
valuesWontReduce VLam (BetaSucc _) impossible
valuesWontReduce VZero (BetaSucc _) impossible
valuesWontReduce (VSucc x) (BetaSucc _) impossible
valuesWontReduce VLam BetaMu impossible
valuesWontReduce VZero BetaMu impossible
valuesWontReduce (VSucc x) BetaMu impossible

reductionsCantBeValues : v ~> v' -> Not (Value v)
reductionsCantBeValues x y = valuesWontReduce y x

data Progress : (m : [] |- a) -> Type where
  Step' : {n : _} -> m ~> n -> Progress m
  Done' : Value m -> Progress m

twoProof : 2 + 2 `Equal` 4
twoProof = Refl

progress : (m : [] |- a) -> Progress m
progress (^ x) impossible
progress (\\ x) = Done' VLam
progress (x |> y) with (progress x)
  progress (x |> y) | (Step' z) = Step' (Xi1 z)
  progress (x |> y) | (Done' z) with (progress y)
    progress (x |> y) | (Done' z) | (Step' w) = Step' (Xi2 z w)
    progress (\\ x |> y) | (Done' z) | (Done' w) = Step' (BetaLam w)
progress Zero = Done' VZero
progress (Succ x) with (progress x)
  progress (Succ x) | (Step' y) = Step' (XiSucc y)
  progress (Succ x) | (Done' y) = Done' (VSucc y)
progress (Case x y z) with (progress x)
  progress (Case x y z) | (Step' w) = Step' (XiCase w)
  progress (Case Zero y z) | (Done' VZero) = Step' BetaZero
  progress (Case Zero y z) | (Done' VLam) impossible
  progress (Case Zero y z) | (Done' _) = ?impossiblePLsFix
  progress (Case (Succ v) y z) | (Done' (VSucc w)) = Step' (BetaSucc w)
  progress (Case (Succ v) y z) | (Done' _) = ?impossiblePlsFixToo
progress (Mu x) = Step' BetaMu

Gas : Type
Gas = Nat

data Finished : (n : gam |- a) -> Type where
  Done : Value n -> Finished n
  OutOfGas : Finished n

data Steps : [] |- a -> Type where
  MkSteps : l ->> n -> Finished n -> Steps l

eval : Gas -> (l : [] |- a) -> Steps l
eval 0 l = MkSteps (ReduceRefl l) OutOfGas
eval (S k) l with (progress l)
  eval (S k) l | (Step' x {n}) with (eval k n)
    eval (S k) l | (Step' x) | (MkSteps y z) = MkSteps (ReduceTrans (Step x) y) z
  eval (S k) l | (Done' x) = MkSteps (ReduceRefl l) (Done x)


sucMu : [] |- NatType
sucMu = Mu (Succ (#0))

evalInf : (Debrujn.eval 3 Debrujn.sucMu) = MkSteps
    (ReduceTrans (Step BetaMu)
    (ReduceTrans (Step (XiSucc BetaMu))
    (ReduceTrans (Step (XiSucc (XiSucc BetaMu)))
    (ReduceRefl (Succ (Succ (subst (Sigma (Mu (Succ (#0)))) (Succ (#0))))))))) OutOfGas
evalInf = Refl


-- evalTwoPlusTwo : (Debrujn.eval 100 (Debrujn.cTwo |> Debrujn.cSuc |> Zero)) ~=~
--                  MkSteps
--                  (ReduceTrans (Step (Xi1 (BetaLam VLam)))
--                  (ReduceTrans (Step (BetaLam VZero))
--                  (ReduceTrans (Step (Xi2 VLam (BetaLam VZero)))
--                  (ReduceTrans (Step (BetaLam (VSucc VZero)))
--                  (ReduceRefl (Succ (Succ Zero))))))) (Done (VSucc (VSucc VZero)))
-- evalTwoPlusTwo = Refl
-}
