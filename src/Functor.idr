module Functor

import Category

record Functor {0 o1, o2 : Type} (cat1 : Category o1) (cat2 : Category o2) where
  constructor MkFunctor

  -- a way to map objects
  F : o1 -> o2

  -- a way to map morphisms
  -- For each morphism in cat1 between objects a and b
  -- we get a morphism in cat2 between the corresponding objects a and b
  -- but mapped to their counterparts in cat2
  m1m2 : {a, b : o1} -> a ~> b -> F a ~> F b

  -- mapping the identity morphism in cat1 results in the identity morphism in cat2
  mapId : {v : o1} -> m1m2 {a=v} {b=v} (id cat1 {v})  = id cat2 {v = (F v)}

  -- composition of hom-sets
  mapHom : {a, b, c : o1} -> {f : (~:>) cat1 a b} -> {g : (~:>) cat1 b c}
      -> m1m2 {a=a} {b=c} ((|:>) cat1 {a} {b} {c} f g)
       = ((|:>) cat2) {a = F a} {b = F b} {c = F c} (m1m2 {a} {b} f) (m1m2 {a=b} {b=c} g)
