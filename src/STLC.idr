module STLC

import Data.Nat
import Equality

%default total

infixr 0 ==> -- function application, like $
infixr 4 ~>  -- Single
infixr 2 ->> -- "Reduces to"
infixr 7 =>> -- Type in Lambda calculus

infix 5 :-

infixl 6 .:
infixr 4 >:
infixl 5 &.
infixl 5 |-

-- Types in Lambda calculus
public export
data Ty : (0 _ : Type) -> Type where
  Unit : Ty b
  (=>>) : Ty b -> Ty b -> Ty b
  Base : base -> Ty base

-- Contexts
public export
data Ctxt : Type -> Type where
  Nil : Ctxt b
  (&.) : Ctxt b -> Ty b -> Ctxt b

public export
data (>:) : {0 base : Type} -> Ctxt base -> Ty base -> Type where
  Z : gam &. a >: a
  S : (0 extra : Ty base) -> gam >: a -> gam &. extra >: a

public export
data (|-) : {0 base : Type} -> Ctxt base -> Ty base -> Type where

  Var : gam >: a ->
     --------------
        gam |- a

  Lam : {a, b : Ty base} ->
        gam &. a |- b ->
     --------------------
        gam |- a =>> b

  App : {a, b : Ty base} ->
         gam |- a  =>> b ->
         gam |- a ->
      ------------------
         gam |- b

public export 0
Subset : (f : a -> b -> Type) -> (g : a -> b -> Type) -> (x : a) -> (y : a) -> Type
Subset f g x y = (0 xsub : b) -> f x xsub -> g y xsub

public export 0
Rename, CtxtMap, Subst: Ctxt base -> Ctxt base -> Type
Rename  gam del = Subset (>:) (>:) del gam
CtxtMap gam del = Subset (|-) (|-) del gam
Subst   gam del = Subset (>:) (|-) del gam

public export
extend : {0 gam, del: Ctxt base}
      -> {b : Ty base}
      -> del      `Rename` gam
      -> del &. b `Rename` gam &. b
extend f b Z = Z
extend f xsub (S _ x) = S _ (f xsub x)

public export
shift: del `Rename` gam &. g ->
       del `Rename` gam
shift f ren Z = f ren (S g Z)
shift f ren (S extra x) = f ren (S g (S extra x))

public export
rename : {0 gam, del: Ctxt base}
      -> del `Rename` gam
      -> del `CtxtMap` gam
rename f b (Var x) = Var (f b x)
rename f (s =>> b) (Lam  x) = Lam (rename (extend f) b x)
rename f b (App x {a} y {b}) = App (rename f (a =>> b) x) (rename f a y)

public export
extendJ : {0 gam, del : Ctxt base} ->(0 b : Ty base) ->
          del      `Subst` gam     ->
          del &. b `Subst` gam &. b
extendJ a sub a Z = Var Z
extendJ b sub a (S b x) =
    rename {del= del &. b} (\v => S {gam=del} {a=v} b) (_) (sub a x)

||| simultaneous substitution
public export
subst : {0 gam, del : Ctxt base} ->
        del `Subst` gam ->
        del `CtxtMap` gam
subst f xsub (Var x) = f _ x
subst f (a =>> b) (Lam x) =
  Lam (subst (extendJ a f) _ x)
subst f xsub (App x y) =
  App (subst f _ x) (subst f _ y)

-- Proofs about renamings etc

public export
prfShift : {0 sub : del `Rename` gam &. g } ->
           (n : gam >: t) ->
           (shift sub) t n === sub t (S g n)
prfShift Z = Refl
prfShift (S extra x) = Refl

public export 0
shiftTwice : {gam, del : Ctxt base} ->
              {sub : del `Rename` gam &. g} ->
              shift (extend (shift sub)) = shift (shift (extend sub))
0
renVar : {val : Ty base} -> {gam, del : Ctxt base} -> {sub : Subst del (gs &. g)} ->
         rename (\v' => S val) g (sub g Z) = Var (S val Z)
renVar {g} with (sub g Z)
  renVar {g = g} | (Var x) = ?nanit
  renVar {g = (a =>> b)} | (Lam x) = ?renVar_rhs_2
  renVar {g = g} | (App x y) = ?renVar_rhs_3

public export 0
renApp : {gam, del : Ctxt base} -> {s, t : Ty base} ->
         {ren : Rename del gam} ->
         (f : gam |- s =>> t) -> (x : gam |- s) ->
         rename ren t (App f x) = App (rename ren (s =>> t) f) (rename ren s x)
renApp _ _ {t = (z =>> w)} = Refl
renApp _ _ {t = (Base z)} = Refl
renApp _ _ {t = Unit} = Refl


public export
Sigma : {0 x, b : Ty base} -> gam |- b -> gam &. b >: x  -> gam |- x
Sigma y Z = y
Sigma y (S _ z) = Var z

public export
(:-) : {gam : _}
    -> {a, b : Ty base}
    -> gam &. b |- a
    -> gam |- b
    -> gam |- a
(:-) x y =
   subst (\c => Sigma y {x=c}) a x


public export
proofApp : (b' : Ty base) ->
           (f : (gam &. a =>> b) |- b')->
           (n : (gam &. a) |- b) ->
           App (Lam f) (Lam n) = subst (\0 c => Sigma (Lam n)) b' f
proofApp (a =>> b) (Var Z) n = ?proofApp_rhs_1
proofApp b' (Var (S _ x)) n = ?proofApp_rhs_34
proofApp (a =>> b) (Lam x) n = ?proofApp_rhs_2
proofApp b' (App x y) n = ?proofApp_rhs_3

public export
data Value : gam |- a -> Type where
  VLam : Value (Lam n)

public export
data (~>) : (t1, t2 : gam |- a) -> Type where
  Xi1 : l ~> l' -> App l m ~> App l' m
  Xi2 : {v : gam |- a =>> b} -> {m, m' : gam |- a} ->
        Value v -> m ~> m' -> App v m ~> App v m'
  BetaLam : {f : gam &. b |- a} -> {v : gam |- b} ->
            Value v -> (App (Lam f) v) ~> (f :- v)

public export
data (->>) : (t1, t2 : gam |- a) -> Type where
  Step : m ~> n -> m ->> n
  ReduceRefl : (t : gam |- a) -> t ->> t
  ReduceTrans : l ->> m -> m ->> n -> l ->> n

export
Reflexive (gam |- a) (->>) where
  reflexive = ReduceRefl x

export
Transitive (gam |- a) (->>) where
  transitive = ReduceTrans

export
Preorder (gam |- a) (->>) where
