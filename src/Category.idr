module Category

import Equality

%default total

infixr 1 ~>
infixr 1 ~:>
infixl 5 |>
infixl 5 |:>
infix 3 <>

infixr 4 >>-
infixr 4 -<<

||| A category is defined by its objects (`o`)and morphisms (`m`)
||| It must provide an identity morphism for each object and a composition
||| operation for composing morphisms. It also must provide proofs that
||| the identity is neutral wrt to composition and that composition is
||| associative.
||| `a`, `b`, `c`, `d` are names for objects
||| `f`, `g`, `h` are names for morphisms
public export
record Category (o : Type) where
  constructor MkCategory

  -- The morphisms
  (~:>) : o -> o -> Type

  -- The identity
  id : (v : o) -> v ~:> v

  -- The composition
  (|:>) : {a, b, c : o} -> (a ~:> b) -> (b ~:> c) -> (a ~:> c)

  -- Proofs of identity left and right
  idLeft : {a, b : o} -> {f : a ~:> b} -> f |:> id b = f
  idRight : {a, b : o} -> {f : a ~:> b} -> id a |:> f = f

  -- Proof of associativity of composition
  compAssoc : {a, b, c, d : o} ->
              {f : a ~:> b} ->
              {g : b ~:> c} ->
              {h : c ~:> d} ->
              f |:> (g |:> h) = (f |:> g) |:> h

-- An operator for sequential composition without passing in the category in argument
public export 0
(|>) : (cat : Category o) => {a, b, c : o} ->
       (~:>) cat a b -> (~:>) cat b c -> (~:>) cat a c
(|>) = Category.(|:>) cat

-- A verbose function for sequential composition
public export 0
Compose : (cat : Category o) => (start, middle, end : o) ->
         (~:>) cat start middle -> (~:>) cat middle end -> (~:>) cat start end
Compose _ _ _ = Category.(|:>) cat

-- An operator for morphisms without passing the category in argument
public export
0 (~>) : (cat : Category o) =>  o -> o -> Type
(~>) = Category.(~:>) cat

mutual
  public export
  data Comp : (cat : Category o) => (a, b : o) -> Type where
    (-<) : (cat : Category o) => (a : o) -> Next a c -> Comp a c
    End : (cat : Category o) => (a : o) -> Comp a a

  public export
  data Next : (cat : Category o) => (a, c : o) -> Type where
    (>-) : (cat : Category o) => {a, b, c : o} -> (f : a ~> b) -> Comp b c -> Next a c

  public export 0
  Start : (cat : Category o) => {a, b : o} -> Comp a b -> a ~> b
  Start (End a) = cat.id a
  Start ((-<) x y {c=k}) = continue y

  public export 0
  continue : (cat : Category o) => {a, b : o} -> Next a b -> a ~> b
  continue ((>-) f (End l) ) = f
  continue ((>-) f x {b=k} ) = f |> Start x

--         direct
--    a ───────────→ d
--    │ ⟍            ↑
--    │   ⟍          │
--    │     ⟍        │
--  f │      dia     │ h
--    │         ⟍    │
--    │           ⟍  │
--    ↓            ↘︎ │
--    b ───────────→ c
--           g
public export
glueTriangles : (cat : Category o) =>
             {a, b, c, d : o} -> {direct : a ~> d} ->
             {f : a ~> b} -> {g : b ~> c} ->
             {dia : a ~> c} -> {h : c ~> d} ->
             Start (a -< f >-
                    b -< g >-
                End c) = dia ->
             Start (a -< dia >-
                    c -< h >-
                End d) = direct ->
             Start (a -< f >-
                    b -< g >-
                    c -< h >-
                End d) = direct
glueTriangles Refl prf1 = trans cat.compAssoc prf1

-- a ────f────→ b
--       =
-- a ────f'───→ b
--
--
-- a ────f────→ b ────g───→ c
--              =
-- a ────f'───→ b ────g───→ c
public export
congMor1 : (cat : Category o) =>
          {a, b, c : o} ->
          {f, f' : a ~> b} ->
          f = f' ->
          (g : b ~> c) ->
          Start (a -< f  >- b -< g >- End c)
      === Start (a -< f' >- b -< g >- End c)
congMor1 Refl g = Refl

-- a ────g────→ b
--       =
-- a ────g'───→ b
--
--
-- a ────f───→ b ────g───→ c
--              =
-- a ────f───→ b ────g'──→ c
public export
congMor2 : (cat : Category o) =>
          {a, b, c : o} ->
          {g, g' : b ~> c} ->
          (f : a ~> b) ->
          g = g' ->
          Start (a -< f >- b -< g >- End c)
      === Start (a -< f >- b -< g' >- End c)
congMor2 f Refl = Refl

--           f1
--    a ───────────→ b
--    │ ⟍            │
--    │   ⟍          │
--    │     ⟍        │
-- f2 │      dia     │ g1
--    │        ⟍     │
--    │          ⟍   │
--    ↓            ↘︎ ↓
--    c ───────────→ d
--           g2
public export
square : (cat : Category o) =>
         {0 a, b, c, d : o} ->
         {0 f1 : a ~> b} ->
         {0 f2 : a ~> c} ->
         {0 g1 : b ~> d} ->
         {0 g2 : c ~> d} ->
         {0 dia : a ~> d} ->
         Start (a -< f1 >- b -< g1 >- End d) = dia ->
         dia = Start (a -< f2 >- c -< g2 >- End d) ->
         Start (a -< f1 >- b -< g1 >- End d) =
         Start (a -< f2 >- c -< g2 >- End d)
square p1 p2 = trans p1 p2

export
forward : (cat : Category o) =>
          {a, b, c, d : o} ->
          {f : a ~> b} ->
          {g : b ~> c} ->
          {h : a ~> c} ->
          {k : c ~> d} ->
          {l : a ~> d} ->
          Compose a b c f g = h ->
          Compose a c d h k = l ->
          Compose a b d f (Compose b c d g k) = l
forward Refl Refl = cat.compAssoc


export
forward' : (cat : Category o) =>
          {a, b, c, d : o} ->
          {f : a ~> b} ->
          {g : b ~> c} ->
          {h : a ~> c} ->
          {k : c ~> d} ->
          {l : a ~> d} ->
          {m : b ~> d} ->
          Compose a b c f g = h ->
          Compose a c d h k = l ->
          Compose b c d g k = m ->
          Compose a b d f m = l
forward' Refl Refl Refl = cat.compAssoc

public export 0
Triangle :
    (cat : Category o) =>
    (a, b, c : o) ->
    (f : a ~> b) ->
    (g : b ~> c) ->
    (h : a ~> c) ->
    Type
Triangle a b c f g h = (f |> g) === h


public export 0
tetrahedron :
    (cat : Category o) =>
    {a, b, c, d : o} ->
    {f : a ~> b} ->
    {g : b ~> c} ->
    {h : a ~> c} ->
    {k : b ~> d} ->
    {m : c ~> d} ->
    {l : a ~> d} ->
    Triangle a b c f g h ->
    Triangle b c d g m k ->
    Triangle a c d h m l ->
    Triangle a b d f k l
tetrahedron Refl Refl Refl = cat.compAssoc

dual : Category o -> Category o
