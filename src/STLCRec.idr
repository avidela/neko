module STLCRec

import Debrujn
import Cartesian
import Category
import Terminal
import Exponential
import Product
import Control.Relation

{-
%default total

STLC : Category (LType base)
STLC = MkCategory
  (\ty1, ty2  => [] &. ty1 |- ty2)
  ?identity
  ?compos
  ?prfIdL
  ?prfIdR
  ?prfCOmp

STLCCartesian : IsCartesianClosed STLC
STLCCartesian = ?STLCCartesian_rhs

  --constructor MkSTLC
  --cat : Category objects
  --prod : HasProduct cat
  --exp : HasExp cat prod
  --term : HasTerminal cat
  --interp : base -> objects
namespace STLC

  0
  TypeToObjs : {0 base, o : Type} ->
               (cat : Category o) => (term : HasTerminal cat) => (interp : base -> o) =>
               (prod : HasProduct cat) => (exp : HasExp cat prod) =>
               LType base -> o
  TypeToObjs (x =>> y) = TypeToObjs y ^ TypeToObjs x
  TypeToObjs (Base x) = interp x
  TypeToObjs Unit = term.terminal

  0
  CtxtToObjs : {0 base, o : Type} ->
               (cat : Category o) => (term : HasTerminal cat) => (interp : base -> o) =>
               (prod : HasProduct cat) => (exp : HasExp cat prod) =>
               Ctxt base -> o
  CtxtToObjs [] = term.terminal
  CtxtToObjs (x :: xs) = CtxtToObjs xs >< TypeToObjs x

  0
  interpVar : {0 base, o : Type} ->
              (cat : Category o) => (term : HasTerminal cat) => (interp : base -> o) =>
              (prod : HasProduct cat) => (exp : HasExp cat prod) =>
              qam >: t -> CtxtToObjs {o} gam ~> TypeToObjs {o} t
  interpVar Z = prod.pi2 _ _
  interpVar (S a) = let rec = interpVar a
                     in prod.pi1 _ _ |> rec

  0
  toArrows : (t : LType base) ->
             (gam : Ctxt base) ->
             gam |- t ->
             ctxtToObjs gam ~> typeToObjs t
  toArrows t gam (T x) = interpVar x
  toArrows _ gam ((\\) x {a} {b} ) =
    let rec = toArrows _ (a :: gam) x in
        exp.lam (ctxtToObjs gam)
                (typeToObjs a)
                (typeToObjs b)
                rec
  toArrows t gam (App x y {a} {b=t}) =
    let ex = exp.eval (typeToObjs t) (typeToObjs a)
        f2 = toArrows _ gam y
        f1 = toArrows _ gam x
        prodMor = f1 -*- f2
     in prodMor |> ex

  0
  interpSubsetC : (gam, del : Ctxt base) ->
                  SubsetC gam del ->
                  ctxtToObjs del ~> ctxtToObjs gam
  interpSubsetC [] ds f = term.toTerm (ctxtToObjs ds)
  interpSubsetC (g :: gs) ds f =
    let v = interpVar (f {xsub = g} Z)
        rec = interpSubsetC gs ds (contract f)
     in rec -*- v

  0
  interpRenameVar : (gam, del : Ctxt base) -> (t : LType base) ->
                    (sub : SubsetC gam del) -> (var : gam >: t) ->
                    Start (ctxtToObjs del -< interpSubsetC gam del sub >-
                           ctxtToObjs gam -< interpVar var >-
                           End (typeToObjs t))
                    = interpVar (sub var)
  interpRenameVar (t :: gam) del t sub Z =
    prodRight prod (interpSubsetC gam del (contract sub))
                   (interpVar (sub {xsub = t} Z))
--
--                iVar (sub (S n))
--                 Δ ───────> t
--                 │╲         ↑
--                 │ ╲        │
--                 │  ╲       │
--                 │   ╲      │
--  prod (Ctxt sub)│    ╲     │ iVar n
--       (iVar n)  │     ╲    │
--                 │      ╲   │
--                 │       ╲  │
--                 v        ⊿ │
--                Γ×g ──────➞ Γ
--                      pi1
  interpRenameVar (g :: gam) del t sub (S n) =
    let botLeft = prodLeft prod (interpSubsetC gam del $ contract sub) (interpVar $ sub Z)
        topRight = interpRenameVar gam del t (contract sub) n
        interpCong = cong interpVar (prfContract {sub} n) in
        glueTriangles {dia= interpSubsetC gam del (contract sub)}
                      botLeft
                      (trans topRight interpCong)
  0
  interpRenamPrf : (gam, del : Ctxt base) -> (t : LType base) ->
                   (sub : SubsetC gam del) -> (term : gam |- t) ->
                   Start (ctxtToObjs del -< interpSubsetC gam del sub >-
                          ctxtToObjs gam -< toArrows t gam term >-
                          End (typeToObjs t))
              = toArrows t del (rename sub term)
  interpRenamPrf (t :: gam) del t sub (T Z)
    = prodRight prod (interpSubsetC gam del (contract sub)) (interpVar (sub Z))

  interpRenamPrf (s :: gam) del t sub (T (S u))
    = let rec = interpRenamPrf gam del t (contract sub) (T u)
          p = prod.prodLeft (interpSubsetC gam del (contract sub)) (interpVar (sub Z))
          interpCong = cong interpVar (prfContract {sub} u) in
          glueTriangles p (trans rec interpCong )
  interpRenamPrf gam del (a =>> b) sub ((\\) body)
      = let rec = interpRenamPrf (a :: gam) (a :: del) b (extend sub) body
            f2 = toArrows b (a :: del) (rename (extend sub) body )
            congExp = (cong (exp.lam (ctxtToObjs del) (typeToObjs a) (typeToObjs b))
                            rec)
            nLam = sym (exp.naturalLam (interpSubsetC gam del sub) (toArrows b (a :: gam) body))
     in let
            end : Start ((ctxtToObjs del >< typeToObjs a) -< prod.pi1 (ctxtToObjs del) (typeToObjs a) >-
                          ctxtToObjs del -< (interpSubsetC gam del sub) >-
                     End (ctxtToObjs gam))
              === interpSubsetC gam (a :: del) (contract (extend sub))
            end = trans ?lul (exp.lamComp _ _ _ (interpSubsetC gam (a :: del) (contract (extend sub))))


-- lul : (.pi1 prod (ctxtToObjs del) (typeToObjs a)) |> (interpSubsetC gam del (sub))
-- ===  (prod.uniqueProduct
--            ((.pi1 prod (ctxtToObjs del) (typeToObjs a)) |>
--             (.lam exp (ctxtToObjs del) (typeToObjs a) (ctxtToObjs gam)
--                       (interpSubsetC gam (a :: del) (contract (extend sub)))))
--            ((.pi2 prod (ctxtToObjs del) (typeToObjs a)) |> cat.id))
--     |> (.eval exp (ctxtToObjs gam) (typeToObjs a))
         in trans nLam (cong (exp.lam (ctxtToObjs del) (typeToObjs a) (typeToObjs b))
                             (trans (congMor (cong2 prod.uniqueProduct
                                                    (end)
                                                    cat.idLeft)
                                             (toArrows b (a :: gam) body))
                                    rec))
-- actual rhs:
--  exp.lam (ctxtToObjs del) (typeToObjs a) (typeToObjs b)
--          ((.uniqueProduct
--                 ((.pi1 (ctxtToObjs del) (typeToObjs a))
--                         |> (interpSubsetC gam del sub))
--                 ((.pi2 (ctxtToObjs del) (typeToObjs a)) |> .id cat))
--           |> c)
--
-- expected rhs:
-- exp.lam (ctxtToObjs del) (typeToObjs a) (typeToObjs b)
--          (toArrows b (a :: del) (rename (extend sub) lam))


-- interpRenamPrf_rhs_3 :
-- (interpSubsetC gam del (sub))
-- |>
-- (toArrows t gam (App x y))
-- =
-- toArrows t del (rename sub (App x y))
  interpRenamPrf gam del b sub (App {a} {b} x y) =
    let v = interpRenamPrf gam del (a =>> b) sub x
        w = interpRenamPrf gam del a sub y
     in ?wot
     where

       glue : Start (ctxtToObjs del -< interpSubsetC gam del sub >-
                     ctxtToObjs gam -< toArrows (a =>> b) gam x >-
                End (typeToObjs (a =>> b)))
              === toArrows (a =>> b) del (rename (sub) x) ->
              Start (ctxtToObjs del -< interpSubsetC gam del (sub) >-
                     ctxtToObjs gam -< toArrows a gam y >-
                End (typeToObjs a))
              === toArrows a del (rename (sub) y) ->
              Start (ctxtToObjs del -< interpSubsetC gam del sub >-
                     ctxtToObjs gam -< toArrows b gam (App x y) >-
                 End (typeToObjs b))
              === toArrows b del (rename (sub) (App x y))
       glue prf prf1 = ?nope

  0
  interpSubsetJ : (gam, del : Ctxt base) ->
                  SubsetJ gam del ->
                  ctxtToObjs del ~> ctxtToObjs gam
  interpSubsetJ [] del f = term.toTerm _
  interpSubsetJ (g :: gs) del f =
    let v = toArrows g del (f {xsub=g} (T Z))
        rec = interpSubsetJ gs del (subst (?wut f))
     in rec -*- v










