module Isomorphism

infix 1 ~~

public export
record (~~) (a, b : Type) where
  constructor MkIso
  to : a -> b
  from : b -> a
  toFrom : (v : a) -> from (to v) = v 
  fromTo : (v : b) -> to (from v) = v
