module Iso

import Control.Relation
import Control.Order

infix 2 ~=

public export
record (~=) (a : Type) (b : Type) where
  constructor MkIso
  from : a -> b
  to : b -> a
  0 fromTo : {x : b} -> from (to x) = x
  0 toFrom : {x : a} -> to (from x) = x

Reflexive Type (~=) where
  reflexive = MkIso id id Refl Refl

Transitive Type (~=) where
  transitive (MkIso ab ba fromToAB toFromBA) (MkIso bc cb fromToBC toFromCB)
      = MkIso (\x => bc (ab x)) (\x => ba (cb x)) fn ?soijo
      where
        0
        fn : {x : z} -> bc (ab (ba (cb x))) = x
        fn = let 0 f' = fromToAB in ?hwiu
        0
        gn : {v : x} -> ba (cb (bc (ab v))) = v
        gn = let 0 g' = toFromCB in let r = replace ?whu ?va in ?end

Preorder Type (~=) where
