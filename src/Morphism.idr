module Morphism

infixr 2 =>>

record (=>>) (a, b : Type) where
  constructor MkMorphism
