module Exponential

import Category
import Product
import Isomorphism

infixl 6 ^^
infixl 6 ^

public export
record HasExp {0 o : Type} (cat : Category o) (prod : HasProduct cat) where
  constructor MkExp
  (^^) : o -> o -> o
  eval : (z, y : o) -> z^^y >< y ~> z
  lam : (x, y, z : o) ->
        (g : (x >< y) ~> z) ->
        x ~> z^^y

  ||| proof that (lam g) -.- id |> eval = g
  lamComp : (x, y, z : o) ->
            (g : (x >< y) ~> z) ->
               Start (
                 (x >< y) -< ((-.-) {a=x} {b=z^^y} {c=y} {d=y} (lam x y z g) (cat.id {v=y})) >-
                 (z ^^ y >< y) -< (eval z y) >-
                 End z)
                === g

  ||| (arrow : x ~> x') -> lam (arrow -.- id) |> g = arrow |> lam g
  naturalLam : {x, x', y, z : o} ->
               (arrow : x ~> x') ->
               (g : (x' >< y) ~> z) ->
               (lam x y z
                     (Start ((x >< y) -< ((-.-) {a=x} {b=x'} {c=y} {d=y} arrow (cat.id {v=y})) >-
                            (x' >< y) -< g >- End z)))
               ===
                 Start (x -< arrow >- x' -< lam x' y z g >- End (z ^^ y))

public export
0 (^) : (cat : Category obj) => (prod : HasProduct cat) => (exp : HasExp cat prod) =>
        obj -> obj -> obj
(^) = (^^) exp
