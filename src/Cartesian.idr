module Cartesian

import Category
import Terminal
import Product
import Exponential

public export
record IsCartesianClosed {0 o : Type} (cat : Category o)  where
  constructor MkCCC
  prod : HasProduct cat
  exp : HasExp cat prod
  term : HasTerminal cat

