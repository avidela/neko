module Product

import Category
import Isomorphism

-- reads as ×
infixl 5 ><
infixl 5 >:<
infixl 5 -.-
infixl 5 -*-

public export
record HasProduct {0 o : Type} (cat : Category o) where
  constructor MkProd

  (>:<) : o -> o -> o
  pi1 : (0 a, b : o) -> a >:< b ~> a
  pi2 : (0 a, b : o) -> a >:< b ~> b

  --              c
  --            ⟋ │ ⟍
  --       f  ⟋   │   ⟍  g
  --        ⟋     │!    ⟍
  --      ⟋       │       ⟍
  --    ↙︎         ↓         ↘︎
  --  a ←────── a × b ──────→ b
  --       π₁            π₂
  --
  prod : {0 a, b, c: o} ->
         c ~> a ->
         c ~> b ->
         c ~> (a >:< b)

  --              c
  --            ⟋ │
  --       f  ⟋   │
  --        ⟋     │!
  --      ⟋       │
  --    ↙︎         ↓
  --  a ←────── a × b
  --       π₁
  --
  -- composing the unique morphism from c to a × b with pi1
  -- is the same as going through f
  prodLeft : {0 a, b, c : o} -> (f : c ~> a) -> (g : c ~> b) ->
             (|>) {a=c} {b=(a >:< b)} {c=a}
                  (prod {a} {b} {c} f g)
                  (pi1 a b)
                = f

  --    c
  --    │ ⟍
  --    │   ⟍  g
  --    │!    ⟍
  --    │       ⟍
  --    ↓         ↘︎
  --  a × b ──────→ b
  --         π₂
  -- composing the unique morphism from c to a × b with π₂
  -- is the same as going through
  prodRight : {0 a, b, c : o} -> (f : c ~> a) -> (g : c ~> b) ->
              (|>) {a=c} {b=(a >:< b)} {c=b}
                   (prod {a} {b} {c} f g)
                   (pi2 a b)
                 = g

  uniq : {0 a, b, c : o} ->
         {f1 : c ~> a} ->
         {f2 : c ~> b} ->
         {p : c ~> a >:< b} ->
         Start (c -< p >- (a >:< b)
                  -< pi1 a b >- End a) === f1 ->
         Start (c -< p >- (a >:< b)
                  -< pi2 a b >- End b) === f2 ->
         prod {a} {b} {c} f1 f2 = p

public export
(><) : (cat : Category o) => (prod : HasProduct cat) => o -> o -> o
(><) = (>:<) prod

public export
swap' : (cat : Category o) => (prod : HasProduct cat) => (a, b : o) -> a >< b ~> b >< a
swap' a b = prod.prod (prod.pi2 a b) (prod.pi1 a b)

public export
swap : (cat : Category o) => (prod : HasProduct cat) => {a, b : o} -> a >< b ~> b >< a
swap = prod.prod (prod.pi2 a b) (prod.pi1 a b)

public export
0 (-.-) : (cat : Category o) => (prod : HasProduct cat) =>
        {a, b, c, d : o} ->
        a ~> b -> c ~> d -> a >< c ~> b >< d
(-.-) m1 m2 = prod.prod (prod.pi1 a c |> m1) (prod.pi2 a c |> m2)

public export
0 (-*-) : (cat : Category o) => (prod : HasProduct cat) =>
        {a, b, c : o} ->
        a ~> b -> a ~> c -> a ~> b >< c
(-*-) = prod.prod

public export 0
Prod : (cat : Category o) => (prod : HasProduct cat) =>
       (a, b, c : o) ->
       a ~> b -> a ~> c -> a ~> b >< c
Prod _ _ _ = prod.prod


--  d1:
--    a
--    │ ⟍
--    │   ⟍  h
--   k│     ⟍
--    │       ⟍
--    ↓         ↘︎
--    b ───────→  c
--         f
--
--  d2:
--    a
--    │ ⟍
--    │   ⟍  l
--   k│     ⟍
--    │       ⟍
--    ↓         ↘︎
--    b ───────→  d
--         g
--  result:
--    a
--    │ ⟍
--    │   ⟍  h -*- l
--   k│     ⟍
--    │       ⟍
--    ↓         ↘︎
--    b ───────→  c >< d
--       f -*- g
export 0
natPairing : (cat : Category o) => (prod : HasProduct cat) =>
             {a, b, c : o} ->
             {k : a ~> b} ->
             {f : b ~> c} ->
             {g : b ~> d} ->
             {h : a ~> c} ->
             {l : a ~> d} ->
             (d1 : Compose a b c k f = h) ->
             (d2 : Compose a b d k g = l) ->
             Compose a b (c >< d) k (Prod b c d f g) = (Prod a c d h l)
natPairing Refl Refl =
  let p = congMor2 k (prod.prodLeft f g)
      q = congMor2 k (prod.prodRight f g)
   in sym $ prod.uniq (trans (sym cat.compAssoc) p)
                      (trans (sym cat.compAssoc) q)

-- 0
-- omegaProof2 : (cat : Category o) => (prod : HasProduct cat) =>
--              {a, b, c : o} ->
--              {k : a ~> b} ->
--              {f : b ~> c} ->
--              {g : b ~> d} ->
--              {h : a ~> c} ->
--              {l : a ~> d} ->
--              {op : c >< d ~> e} ->
--              (d1 : Compose a b c k f = h) ->
--              (d2 : Compose a b d k g = l) ->
--              Compose a b (c >< d) k (Prod b c d f g) = (Prod a c d h l)
-- omegaProof2 Refl Refl =
--   let p = congMor2 k (prod.prodLeft f g)
--       q = congMor2 k (prod.prodRight f g)
--    in sym $ prod.uniq (trans (sym cat.compAssoc) p)
--                       (trans (sym cat.compAssoc) q)


-- 0
-- congProd2 : (cat : Category o) =>
--             (prod : HasProduct cat) =>
--             {a, b, c, d : o} ->
--             (fg : (a >< c) ~> (b >< d)) ->
--             (f : a ~> b) -> (g : c ~> d) ->
--             fg === (-.-) {a} {b} {c} {d} f g
-- congProd2 fg f g =
--   let l = prod.prodLeft fg ?que
--       r = prod.prodRight (Compose (a >< c) a b (prod.pi1 a c) f)
--                          (Compose (a >< c) c d (prod.pi2 a c) g)
--       s = sym $ prod.uniq (trans ?a ?c) (trans (congMor2 ?wut) r) in s
