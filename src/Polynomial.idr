module Polynomial

import Data.Fin
import Data.Fun

fork : (a -> b) -> (a -> d) -> a -> (b, d)
fork f g x = (f x, g x)

infix 3 /-\

(/-\) : (a -> b) -> (a -> d) -> a -> (b, d)
f /-\ g = \x => (f x, g x)

data L a b = Nil | Cons a b

Bifunctor L where
  bimap f g Nil = Nil
  bimap f g (Cons x y) = Cons (f x)  (g y)

data Mu3 : (f : Type -> Type -> Type -> Type) -> Type -> Type where
  In3 : {0 f : Type -> Type -> Type -> Type} -> (f a a (Mu3 f a)) -> Mu3 f a

data Mu2 : (f : Type -> Type -> Type) -> Type -> Type where
  In2 : {0 f : Type -> Type -> Type} -> (f a (Mu2 f a)) -> Mu2 f a

data Mu : (f : Type -> Type) -> Type -> Type where
  In : {0 f : Type -> Type} -> (f (Mu f a)) -> Mu f a


TypesN : Nat -> Type
TypesN n = Fun (replicate n Type) Type

app : Fun (v :: vs) r -> v -> Fun vs r
app f x = f x

Ty : Vect (S n) Type -> Type
Ty [x] = x
Ty (x :: y :: xs) = (x, Ty (y :: xs))

appN : {vs : _} -> Fun (vs ++ ws) r -> Ty vs -> Fun ws r
appN f x {vs = (z :: [])} = f x
appN f x {vs = (z :: (w :: xs))} = appN (f (fst x)) (snd x)

appAll : {vs : _} -> Fun vs r -> Ty vs -> r
appAll f y {vs = (z :: [])} = f y
appAll f y {vs = (z :: (w :: xs))} = appAll (f (fst y)) (snd y)

AppTypes : {n : Nat} -> (a : Type) -> TypesN (S (S n)) -> Type -> Type
AppTypes {n = 0} a f x = f a x
AppTypes {n = (S k)} a f x = AppTypes {n = k} a (f a) x

data MuN : (n : Nat) -> TypesN (S $ S n) -> Type -> Type where
  InN : (n : Nat) -> {0 a : Type} -> {0 f : TypesN (S $ S n)} ->
        AppTypes a f (MuN n f a)
        -> MuN n f a

record FunctorN (n : Nat) (f : TypesN (S n)) where
  constructor MkFuncN
  n_map : (args : Ty (replicate (S n) Type)) ->
          (brgs : Ty (replicate (S n) Type)) ->
          appAll {vs=replicate (S n) Type} f args ->
          appAll {vs=replicate (S n) Type} f brgs


cata : Functor f => (f a -> a) -> Mu f a -> a
cata g (In x) = g (map (cata g) x)

List : Type -> Type
List x = Mu2 L x

cata2 : (bi : Bifunctor f) => (f a b -> b) -> Mu2 f a -> b
cata2 phi (In2 x) = phi (bimap id (cata2 phi) x)

cataN : {0 n : Nat} -> {0 f : TypesN (S n)} -> (fn : FunctorN n f) => ?whu

aList : Polynomial.List Nat
aList = In2 $ Cons 2 $ In2 $ Cons 1 $ In2 $ Cons 0 $ In2 $ Nil

sum : Nat
sum = cata2 (\case [] => 0
                   (Cons x y) => x + y) aList

test : Polynomial.sum = 3
test = Refl

Bifunctor f => Functor (Mu2 f) where
  map f (In2 x) = In2 (bimap f (map f) x)

data Nu2 : (f : Type -> Type -> Type) -> Type -> Type where
  Out2 : Inf (f a (Nu2 f a)) -> Nu2 f a

data Nu : (f : Type -> Type) -> Type where
  Out : Inf (f (Nu f)) -> Nu f

ana2 : Bifunctor f => (b -> f a b) -> b -> Nu2 f a
ana2 phi x = Out2 (bimap id (ana2 phi) (phi x))

Colist : Type -> Type
Colist a = Nu2 L a

data U a b = Empty | Fork b a b

range : (Int, Int) -> Colist Int
range = ana2 next where
  next : (Int, Int) -> L Int (Int, Int)
  next (lower, upper) = if lower == upper then Nil else Cons lower (lower + 1, upper)

ana : Functor f => (b -> f b) -> b -> Nu f
ana phi x = Out (map (ana phi) (phi x))

-- build : Prelude.List a -> Colist a
-- build = ana next where
--   next : Polynomial.List a -> U a (Polynomial.List a)

Conat : Type
Conat = Nu Maybe

Zero : Conat
Zero = Out Nothing

Succ : Conat -> Conat
Succ x = Out $ Just x

unfold : (b -> Maybe b) -> b -> Conat
unfold phi x = case phi x of Nothing => Zero
                             (Just y) => Succ (unfold phi y)

para : Bifunctor f => (f a (b, Mu2 f a) -> b) -> Mu2 f a -> b
para phi (In2 x) = phi (bimap id (para phi /-\ id) x)

data Tree a = Leaf a | Branch (Tree a) (Tree a)

height : Tree a -> Nat

perfect : Tree a -> Bool
perfect (Leaf x) = True
perfect (Branch x y) = perfect x && perfect y && (height x == height y)
                                                -- ^ that's kinda cringe

ph : Tree a -> (Bool, Nat)
ph (Leaf _) = (True, 1)
ph (Branch x y) = let (x1, h1) = ph x
                      (x2, h2) = ph y
                  in (x1 && x2 && h1 == h2, S (max h1 h2))

zygo' : Bifunctor f => (f a (b, c) -> b) -> (f a c -> c) -> Mu2 f a -> (b, c)
zygo' phi psi (In2 x) = let x' = (bimap id (zygo' phi psi) x)
                         in (phi x', psi (bimap id snd x'))

zygo : Bifunctor f => (f a (b, c) -> b) -> (f a c -> c) -> Mu2 f a -> b
zygo phi psi = fst . zygo' phi psi

-- Every para is a zygo with id
-- every zygo is a post-processed cata

mutu : Bifunctor f => (f a (b, c) -> b) -> (f a (b, c) -> c) -> Mu2 f a -> b


-- Cofree comonad

record Cofree (f : Type -> Type) (b : Type) where
  constructor Node
  root : b
  subs : f (Cofree f b)


histo : Bifunctor f => (f a (Cofree (f a) b) -> b) -> Mu2 f a -> b
histo phi = root . cata2 (\x => Node (phi x) x)


