module Sequent

import Data.Fin

infixr 3 =>>
infixr 7 ><
infix 2 :-
infix 2 |-

data Ty a = Builtin a
          | (><) (Ty a) (Ty a)
          | (=>>) (Ty a) (Ty a)

data Lang : Type where
  Var : Nat -> Lang
  Lam : Lang -> Lang
  MkProd : Lang -> Lang -> Lang
  Proj1 : Lang -> Lang
  Proj2 : Lang -> Lang
  App : Lang -> Lang -> Lang

record Term (a : Type) where
  constructor (:-)
  term : Lang
  type : Ty a

data (|-) : SnocList (Term a) -> (Term a) -> Type where
  Id    : (xs :< x) |- x
  AndR  : g |- (m :- t1) -> g |- (n :- t2) -> g |- (MkProd m n :- t1 >< t2)
  AndL1 : g :< (m :- a >< b) :< (Proj1 m :- a) |- c
       -> g :< (m :- a >< b) |- c
  AndL2 : g :< (m :- a >< b) :< (Proj1 m :- b) |- c
       -> g :< (m :- a >< b) |- c
  ImpR  : g :< (a :- t1) |- (b :- t2)
       -> g |- (Lam b :- t1 =>> t2)
  ImpL  : g :< (m :- a =>> b) |- (n :- a)
       -> g :< (m :- a =>> b) :< (App m n :- b) |- c
       -> g :< (f :- a =>> b) |- c


ex1 : [< m :- a =>> b =>> c ] |- (Lam (App (App m (Proj1 (Var 0))) (Proj2 (Var 0))) :- (a >< b) =>> c)
ex1 = ImpR (AndL1 (?ahui))

