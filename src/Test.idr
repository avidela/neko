module Test

import Data.Nat
import Control.Relation

unicity : (prf1, prf2 : n `LTE` m) -> prf1 = prf2
unicity LTEZero LTEZero = Refl
unicity (LTESucc prf1) (LTESucc prf2) = rewrite unicity prf1 prf2 in Refl

likeSo : {l1 : n `LTE` m} -> Relation.transitive l1 Relation.reflexive = l1
likeSo = irrelevantEq $ unicity (transitive l1 reflexive) l1

