# Neko - Category-theory for functional programmers

Neko is a category theory library focused on tutorials and learning. It showcases uses-cases
for category theory, examples and tutorials.
